class Admin < ActiveRecord::Base

  rolify

  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable

end
