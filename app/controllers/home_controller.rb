class HomeController < ApplicationController
  def index
    if admin_signed_in?
      redirect_to home_dashboard
    else
      redirect_to admin_session_path
    end
  end
end
